import static org.junit.Assert.*;

public class ScoreCalculatorTest {

    @org.junit.Test
    public void calculateScoreEmpty() {
        int[][] TestScores = {{0,0,0},{0,0,0},{0,0,0}};
        int[] result = ScoreCalculator.calculateScore(TestScores);
        ScoreCalculator.printArray(result);
        assert(result.length == 3);
        assert(result[0] == 0);
        assert(result[1] == 0);
        assert(result[2] == 0);
    }

    @org.junit.Test
    public void calculateScoreSimple() {
        int[][] TestScores = {{0,1},{3,0}};
        int[] result = ScoreCalculator.calculateScore(TestScores);
        ScoreCalculator.printArray(result);
        assert(result.length == 2);
        assert(result[0] == 1);
        assert(result[1] == 3);
    }

    @org.junit.Test
    public void calculateScoreIII() {
        int[][] TestScores = {{0,1,2},{3,0,4},{5,6,0}};
        int[] result = ScoreCalculator.calculateScore(TestScores);
        ScoreCalculator.printArray(result);
        assertEquals(3, result.length);
        assertEquals(2, result[0]);
        assertEquals(4, result[1]);
        assertEquals(6, result[2]);
    }

    @org.junit.Test
    public void calculateScoreIV() {
        int[][] TestScores = {{0,0,0},{1,0,0},{1,0,0}};
        int[] result = ScoreCalculator.calculateScore(TestScores);
        ScoreCalculator.printArray(result);
        assertEquals(3, result.length);
        assertEquals(2, result[0]);
        assertEquals(3, result[1]);
        assertEquals(3, result[2]);
    }
}