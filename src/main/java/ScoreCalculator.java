public class ScoreCalculator {

    public static int[] calculateScore(final int[][] scoreBoard){
        int NumberOfPlayers = scoreBoard.length;
        int[] scores = new int[NumberOfPlayers];
        int score = 0;

        for (int y = 0; y < NumberOfPlayers; y++){
            for (int x = 0; x < NumberOfPlayers; x++){
                if(x != y){
                    if(scoreBoard[y][x] > scoreBoard[x][y]){
                        score++;
                    }
                    if((scoreBoard[y][x] >= scoreBoard[x][y]) && scoreBoard[y][x] > 0){
                        score++;
                    }
                    if(scoreBoard[y][x] != 0 || scoreBoard[x][y] != 0){
                        score++;
                    }
                }
            }
            scores[y] = score;
            score = 0;
        }
        return scores;
    }

    public static void printArray(int[] arr){
        for(int i = 0; i < arr.length; i++){
            System.out.println(i + ":" + arr[i]);
        }
    }

}


