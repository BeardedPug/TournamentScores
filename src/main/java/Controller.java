import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Controller {

    static String[] players;
    static int[][] scores;
    static int[] outcome;

    public static void main(String [] args){
        if(args.length == 0){
            System.out.println("You must specify csv file location as an argument");
        } else {
            loadCSV(args[0]);
            outcome = ScoreCalculator.calculateScore(scores);
            printScoreBoardAsMd(scores);
            //printScoreBoard(scores);
            printOutcome(outcome);
        }
    }

    static void loadCSV(String file){
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(file));
            line = br.readLine();
            String[] cells = line.split(cvsSplitBy);
            players = new String[cells.length-1];
            for(int i = 1; i < cells.length; i++){
                players[i-1] = cells[i];
            }
            scores = new int[cells.length-1][cells.length-1];
            for(int y = 1; y < cells.length; y++){
                line = br.readLine();
                cells = line.split(cvsSplitBy);
                for(int x = 1; x < cells.length; x++){
                    if(y != x) {
                        scores[y - 1][x - 1] = Integer.parseInt(cells[x]);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void printOutcome(int[] arr){
        for(int i = 0; i < arr.length; i++){
            System.out.println(players[i] + ":" + arr[i]);
        }
    }

    public static void printScoreBoard(int[][] arr){
        int x;
        System.out.print("|"+spacer("")+"|");
        for(x = 0; x < arr.length; x++){
            System.out.print(spacer(players[x])+"|");
        }
        System.out.println(spacer("Scores")+"|");
        System.out.print("|");
        for(x = 0; x < arr.length+2; x++){
            System.out.print("----------|");
        }
        System.out.println();
        for(int y = 0; y < arr.length; y++){
            System.out.print("|"+spacer(players[y])+"|");
            for(x = 0; x < arr.length; x++){
                if(x==y){
                    System.out.print(spacer("X")+"|");
                } else {
                    System.out.print(spacer(arr[y][x]) + "|");
                }
            }
            System.out.println(spacer(outcome[y])+"|");
            for(x = 0; x < arr.length+1; x++){
                System.out.print("------------");
            }
            System.out.println();
        }
    }

    public static void printScoreBoardAsMd(int[][] arr){
        int x;
        System.out.print("|"+spacer("")+"|");
        for(x = 0; x < arr.length; x++){
            System.out.print(spacer(players[x])+"|");
        }
        System.out.println(spacer("Scores")+"|");
        System.out.print("|");
        for(x = 0; x < arr.length+2; x++){
            System.out.print(":--------:|");
        }
        System.out.println();
        for(int y = 0; y < arr.length; y++){
            System.out.print("|"+spacer(players[y])+"|");
            for(x = 0; x < arr.length; x++){
                if(x==y){
                    System.out.print(spacer("X")+"|");
                } else {
                    System.out.print(spacer(arr[y][x]) + "|");
                }
            }
            System.out.println(spacer(outcome[y])+"|");
        }
    }

    public static String spacer(Object obj){
        return String.format("%1$"+10+ "s", obj);
    }
}
